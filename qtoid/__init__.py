from pyramid.config import Configurator
from pyramid.response import Response

from qtoid.public import Root
from qtoid.view import api_renderer

def main(global_config, **settings):
  config = Configurator(settings=settings, root_factory=Root)
  config.add_renderer('api_renderer', api_renderer)
  config.add_static_view('static', 'static', cache_max_age=3600)
  config.scan()
  return config.make_wsgi_app()
