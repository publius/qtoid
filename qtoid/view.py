from pyramid.renderers import JSON

from qtoid.jsend import *

from datetime import datetime
from bson import ObjectId

api_renderer = JSON()
api_renderer.add_adapter(datetime, lambda dt, req: dt.isoformat())
api_renderer.add_adapter(ObjectId, lambda oid, req: str(oid))
api_renderer.add_adapter(set, lambda oid, req: list(oid))


class View(object):
  
  VALID_HTTP_METHOD = set(['GET','PUT','POST','DELETE','COPY'])

  def __init__(self, ctx, req):
    self.ctx = ctx
    self.req = req

  def __call__(self):
    if hasattr(self.ctx, 'error'):
      if self.ctx.error:
        return self.ctx.error
    method = self.req.method
    if method in View.VALID_HTTP_METHOD:
      handler = getattr(self, self.req.method, None)
      if handler:
        return handler(self.req, self.ctx)
    # method not supported or illegal:
    return Error('HTTP {0} method not supported'.format(method))


class validator(object): # NOTE DEPRECATED
  
  def __init__(self, params={}, body={}, post={}, headers={}):
    self.params = params.items()
    self.body = body.items()
    self.post = post.items()
    self.headers = headers.items()
  
  def __call__(self, f):
    def g(_self):
      req = _self.req
      if self.params:
        if not req.params:
          req.context.error = Error('null params')
          return f(_self)
        err = self.validate(req.params, self.params, 'params')
        if err:
          req.context.error = Failure(err)
          return f(_self)
      if self.post:
        if not req.POST:
          req.context.error = Error('null POST')
          return f(_self)
        err = self.validate(req.POST, self.post, 'POST')
        if err:
          req.context.error = Failure(err)
          return f(_self)
      if self.body:
        if not req.content_length:
          req.context.error = Error('null data')
          return f(_self)
        try:
          err = self.validate(req.json, self.body, 'body')
          if err:
            req.context.error = Failure(err)
            return f(_self)
        except ValueError as e:
          req.context.error = Error('JSON decode error')
          return f(_self)
      if self.headers:
        err = self.validate(req.headers, self.headers, 'headers')
        if err:
          req.context.error = Failure(err)
          return f(_self)
      return f(_self)
    return g

  @staticmethod
  def validate(obj, schema, region):
    for k1, v1 in schema:
      v2 = obj.get(k1)
      if k1 not in obj:
        return {k1:'not found in {0}'.format(region)}
      if not isinstance(v2, v1):
        t1, t2 = v1.__name__, v2.__name__
        return {k1:'expected {0} but got {1} in {2}'.format(t1,t2,region)}
       
