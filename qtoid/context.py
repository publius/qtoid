import re

import mongo

class Context(object):
  
  mongo = mongo
  db = mongo.db

  def __init__(self, req, key=None):
    self.req = req 
    self.key = key
    self.error = None

class regexp(object):

  def __init__(self, pattern, *flags):
    self._re = re.compile(pattern, *flags)

  def __call__(self, f):
    def g(*args, **kwargs):
      m = self._re.match(args[1])
      args[0].matchdict = m.groupdict() if m else {}
      return f(*args, **kwargs)
    return g
