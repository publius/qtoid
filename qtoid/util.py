'''qtoid.util'''

import string
import numpy

class validator(dict):
  
  def get(self, key, default=None, dtype=None):
    value = super(validator, self).get(key)
    if value is not None:
      if dtype is not None:
        if not isinstance(value, dtype):
          return value
      return value
    return default

def randstr(n, alpha=numpy.array(list(string.letters+'0123456789-'))):
  return ''.join(numpy.random.choice(alpha, n))

def base36encode(number, alphabet='0123456789abcdefghijklmnopqrstuvwxyz'):
  ''' http://en.wikipedia.org/wiki/Base_36#Python_Conversion_Code
  '''
  base36 = ''
  sign = ''
  if number < 0:
    sign = '-'
    number = -number
  if 0 <= number < len(alphabet):
    return sign + alphabet[number]
  while number != 0:
    number, i = divmod(number, len(alphabet))
    base36 = alphabet[i] + base36
  return sign + base36

def base36decode(number):
  return int(number, 36)

