from ConfigParser import ConfigParser

import cqlite as cql
from   cqlite import util, Error

__all__ = [
  'config',
  'keyspace', 
  'servers', 
  'credentials', 
  'ks', 
]

'''
  This file contains Cassandra global variables, including:
    - ks: top-level connection pool for keyspace
    - config: the Cassandra INI ConfigParser.
    - keyspace: string name of keyspace.
    - servers: a list of pairs, (hostname:str, port:int).
    - credentials: a dict of {user, password}.
'''

def initialize():
  global config, ks
  if not ks.exists:
    ks.create(
      replication={
        'class': 'SimpleStrategy',
        'replication_factor': 1
      }
    )
  ks.fill()
  tables = ks.tables
  for section in config.sections():
    chunks = section.split(':')
    if len(chunks) == 2:
      prefix, table_name = chunks
      table_name = table_name.strip()
      if table_name in tables:
        continue
      if prefix == 'table':
        primary_key = config.get(section, 'key')
        if config.has_option(section, 'options'):
          options = config.get(section, 'options')
        else:
          options = {}
        columns = []
        for col_def in config.get(section, 'columns').split('\n'):
          col_def = col_def.strip()
          if col_def:
            col_name, col_type = col_def.strip().split(':')  
            columns.append('{0} {1}'.format(col_name, col_type))
        # TODO: parse table options
        ks.execute('''
          CREATE TABLE {table_name} (
            {column_section},
            PRIMARY KEY ({primary_key})
        )'''.format(
          table_name=table_name,
          column_section=','.join(columns),
          primary_key=primary_key)
        )
        if config.has_option(section, 'indices'):
          for line in config.get(section, 'indices').strip().split('\n'):
            index_name, columns = line.split(' ', 1)
            ks.execute('''
              CREATE INDEX {index_name} ON {table_name} {columns}
              '''.format(
                index_name=index_name, 
                table_name=table_name, 
                columns=columns
            ))

## Globals --------------------------------------------------------------------#

config = ConfigParser()
config.read('cassandra.ini')

keyspace = config.get('deployment', 'keyspace')

servers = []
for s in config.get('deployment', 'servers').split():
  host, port = s.strip().split(':')
  servers.append((host, int(port)))

credentials = dict(
  username=config.get('deployment', 'user'),
  password=config.get('deployment', 'password'))

ks = cql.Keyspace(
  keyspace,
  servers,
  credentials=credentials,
  max_conns=config.getint('deployment', 'max-pool-size'),
  fill=False,
)

execute = ks.execute # aliases for convenience
cursor = ks.cursor

## Create keyspace and tables if not done so already
initialize()


