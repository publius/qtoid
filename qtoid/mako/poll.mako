<%inherit file="base.mako"/>
<%namespace file="styles.mako" name="styles"/>
<%namespace file="highcharts.mako" name="highcharts"/>
<%!
import random
from itertools import izip

def votezip(poll):
  print poll['vote_names']
  return izip(poll['vote_names'], poll['vote_counts'])
%>

<%block name="stylesheets">
  ${parent.stylesheets()}
  ${styles.stylesheet("poll")}
</%block>

<%block name="scripts">
${parent.scripts()}
<script type="text/javascript">
$(document).ready(function() {
  function done(obj, countElem) {
    $(countElem).text(obj.count)
  }
  $('.choice-wrapper').click(function(e) {
    var choiceElem = $(this).find('.choice-count');
    console.log($(this).children('.choice-text').text());
    $.ajax({
      type: 'POST',
      url: window.location.pathname, 
      data: { e:'vote', text:$(this).children('.choice-text').text() },
      choiceWrapper: this,
      dataType: 'json',
      success: function(obj) {
        done(obj, choiceElem);
      }
    });
  });
});
</script>
</%block>

%if poll:
  <div id="poll-wrapper">
  <%
  poll['vote_counts'] = list(poll['vote_counts'])
  for i in range(len(poll['vote_counts'])):
    poll['vote_counts'][i] = random.randint(400,1200)
  %>
    <p id="poll-text">${poll['prompt'] | n}</p>
    %for name, count in votezip(poll):
    <a class="choice-wrapper">
      <p class="choice-text">${name}</p>
      <p class="choice-count">${count}</p>
    </a>
    %endfor
  </div>
  <div class="hbar"></div>
  %if sum(poll['vote_counts']):
    <% votes = dict([(k,v) for k,v in votezip(poll) if v > 0]) %>
    ${highcharts.pie_chart_of_votes(votes)}
  %endif
%endif
  

