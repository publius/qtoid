<%namespace file="styles.mako" name="styles"/>
<!doctype HTML>
<html>
<head>
  <meta name="description" content="TODO"> 
  <meta http-equiv="content-type" content="text/html;charset=UTF-8">
  <title>Qtoid ${'- '+title if hasattr(self, 'title') else ''}</title>
  <%block name="scripts">
  <script src="/static/js/jquery-1.10.1.min.js" type="text/javascript"></script>
  <script src="/static/js/highcharts.js" type="text/javascript"></script>
  </%block>
  <%block name="stylesheets">
  <link href='http://fonts.googleapis.com/css?family=Glegoo|Average|Fauna+One' rel='stylesheet' type='text/css'>
  ${styles.stylesheet("qtoid")}
  </%block>
</head>
<body>
  <div id="topnav-wrapper">
    <span>Qtoid</span>
  </div>
${self.body()}
</body>
</html>
