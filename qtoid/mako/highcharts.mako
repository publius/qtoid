<%!
import random
import math 
import json

rand_id = lambda: hex(random.randint(1,100000))[2:]

%>

<%def name="single_line_graph(
  time, 
  series, 
  title='null', 
  ytitle='',
  ylabels=[],
  id=rand_id()
)">
<div class="graph" id="${id}"></div>
<script type="text/javascript">
$('#${id}').highcharts({
    chart: {
      // TYPE GOES HERE OR DEFAULTS TO LINE TYPE
    },
    title: {
      text: '${title}',
      style: {
        fontFamily: 'Fauna One',
        fontSize: '10pt',
        color: '#111',
      }
    },
    legend: {
      symbolPadding: 8,
      borderWidth: 0,
      itemStyle: {
        fontSize: '10pt',
        color: '#333',
      }
    },
    yAxis: {
    %if ylabels:
      categories: ${ylabels},
    %endif
      title: {
      text: '${ytitle}',
        style: {
          fontSize: '8pt',
          fontWeight: 'normal',
          color: '#333',
        }
      }
    },
    xAxis: {
      //categories: ${time},
    },
    credits: {
      enabled: false
    },
    series: [
      {
        name: '${title}',
        data: ${series},
        marker: {
          enabled: false,
          states: {
            hover: {
              enabled: true
            }
          }
        }
      },
    ]
});
</script>
</%def>

<%def name="pie_chart_of_votes(
  vote2count,
  title='',
  id=rand_id()
)">
<div class="graph" id="${id}"></div>
<script type="text/javascript">
$('#${id}').highcharts({
    chart: {
      // TYPE GOES HERE OR DEFAULTS TO LINE TYPE
    },
    title: {
      text: '${title}',
      style: {
        fontFamily: 'Fauna One',
        fontSize: '10pt',
        color: '#111',
      }
    },
    plotOptions: {
      pie: {
        allowPointSelect: true,
        cursor: 'pointer',
        borderWidth: 1,
        dataLabels: {
          enabled: true,
          connectorColor: '#666666',
          format: '{point.name}: {point.percentage:.1f} %',
          color: '#333',
          style: {
            fontFamily: 'Fauna One',
            fontSize: '10pt',
          },
        }
      }
    },
    credits: {
      enabled: false
    },
    series: [{
      type: 'pie',
      //name: null,
      data: ${json.dumps(vote2count.items()) | n}
    }]
});
</script>
</%def>
