<%inherit file="base.mako"/>
%if poll:
  <h1>${poll['title']}</h1>
  <p>${poll['prompt']}</p>
  %for caption, count in poll['choices'].iteritems():
    <span style="border:1px solid black; padding: 10px;">${caption}:${count}</span>
  %endfor
%endif
