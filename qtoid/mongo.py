from   ConfigParser import ConfigParser
from   pymongo.errors import PyMongoError
from   pymongo import errors
import pymongo

config = ConfigParser()
config.read('mongo.ini')

# system manager
client = pymongo.MongoClient(
  host=config.get('client', 'host'),
  port=int(config.get('client', 'port')),
  max_pool_size=int(config.get('client', 'max-pool-size')),
  document_class=dict)

# qtoid main database descriptor
db = client[config.get('client', 'database')]

# for each collection section:
for section in config.sections():
  if section.startswith('collection:'):
    collection = section.split(':')[1]
    # ensure indices for this collection:
    if config.has_option(section, 'indices'):
      indices = config.get(section, 'indices').strip().split('\n')
      for idx in indices:
        db[collection].ensure_index(idx.strip())



