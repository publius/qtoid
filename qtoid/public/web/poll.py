from datetime import datetime, timedelta
from collections import defaultdict
from pprint import pprint
import uuid
import json
import urllib
import re

import cqlite
import cqlite.util

from pyramid.response import Response
from pyramid.view import view_config
from pyramid.httpexceptions import HTTPTemporaryRedirect

from   qtoid.view import validator
import qtoid.mongo as mongo
import qtoid.cassandra as cas
import qtoid.context
import qtoid.view 


class Context(qtoid.context.Context):
  
  def __getitem__(self, uri):
    self.fetch_poll(uri)
    if self.poll:
      return self
    else:
      #TODO: redirect to a "create a question" page that defaults
      # to the unknown URI as the title
      return HTTPTemporaryRedirect(location='/')
  
  def fetch_poll(self, uri):
    cur = cas.ks.execute('''
      SELECT * FROM polls WHERE uri = :uri;
    ''', uri=uri)
    try:
      self.poll = cur.documents.next()
    except StopIteration:
      return {}


@view_config(
  context=Context, 
  renderer='poll.mako'
)
class View(qtoid.view.View):
  
  def GET(self):
    poll = self.ctx.poll
    return dict(
      poll=poll,
    )
  
  def POST(self):
    if self.req.POST.get('e') == 'vote':
      res, count = self.register_vote()
      if res.body:
        self.insert_vote_in_time_series(count)
      return res

  ## Auxiliary methods --------------------------------------------------------#

  def register_vote(self):
    vote_name = self.req.POST.get('text').strip()
    res = Response(content_type='application/json')
    poll = self.req.ctx.poll
    count = 0
    if vote_name in poll['vote_names']:
      try:
        count = mongo.db.polls.vote_sets.find_and_modify(
          {'_id':self.ctx.poll['_id']},
          {'$inc':{text:1}},
          upsert=False,
          new=True
        )[text]
        res.body = json.dumps({'count':count})
      except:
        res.body = ''
    else:
      res.body = ''
    return (res, count)
  
  def insert_vote_in_time_series(self, count):
    # returning None signals to emit null body in ajax response
    ctx = self.req.context
    user = uuid.uuid4() # TODO: get real user id
    try:
      cass.pool.execute('''
        INSERT INTO tseries (p, t, n, v, u) 
        VALUES ('{p}', '{t}', {n}, '{v}', {u})
      '''.format(
        p=str(ctx.poll['_id']),
        t=datetime.now().strftime('%Y-%m-%d %H:%M:%S'),
        u=str(user),
        v=self.req.POST['text'],
        n=count,
      ))
    except Exception as e:
      print 'error'.center(60, '#')
      print e

