from pyramid.response import Response
from pyramid.view import view_config

from qtoid.context import Context, regexp
from qtoid.public  import web
from qtoid.public  import api
from qtoid.jsend   import *

class Root(Context):
  
  def __init__(self, req):
    self.req = req

  @regexp(r'api-v(?P<v>\d)/?')
  def __getitem__(self, key):
    v = self.matchdict.get('v')
    if v == '1':
      return api.v1.root.Context(self.req, v)
    else:
      key = key.lower() 
      module = getattr(web, key, None)
      if module:
        # key should match a module name
        return module.Context(self.req, key)
      else:
        return self # go nowhere


@view_config(context=Root, renderer='doc-root.mako')
def root(req):
  return {}
