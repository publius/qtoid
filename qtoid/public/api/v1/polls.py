'''module: qput.pub.api.v1.polls'''

import re
import os
import cgi
import json
import itertools
from   urllib import urlencode
from   pprint import pprint
from   datetime import datetime
from   collections import deque
from   pyramid.view import view_config
from   qtoid.public.api.context import APIContext
from   qtoid.util import randstr, base36encode
from   qtoid import jsend
from   qtoid import mongo
from   qtoid import cassandra
from   qtoid import zk
import qtoid.view

''' TODO: 
  - Write DB errors to a log.
  - Move all transactional logic to module in API pkg
'''

RE_URI = re.compile(r'([\w\-]+)\-(\d+)')
RE_WHITESPACE = re.compile(r'\s+')
RE_NONWORD = re.compile(r'[^\w\-\s]+')
RE_RAW_HTAG1 = re.compile(r'(#\w+)')
RE_RAW_HTAG2 = re.compile(r'(#\([\w\s]+\))')
RE_HTAG1 = re.compile(r'#(\w+)')
RE_HTAG2 = re.compile(r'#\(([\w\s]+)\)')

HTAG_SPAN_FSTR = '<span class="ht">#</span><a href="/search?{0}">{1}</a>'

MAX_URI_LENGTH = 80


class Context(APIContext):
  
  def __getitem__(self, key):
    if not self.error:              # <- error in __init__?
      TIMEOUT = 2                   # <- zk lock acquire timeout
      match = RE_URI.match(key) 
      if match is not None:
        uri1 = match.group()
        try:
          zkfs = zk.fs()
          lock = zkfs.lock(('uris', uri1))
          lock.acquire(timeout=TIMEOUT)

          # fetch poll from cassandra 
          cur = self.polls = cassandra.execute( '''
            SELECT * FROM polls WHERE uri1 = :uri1
          ''', uri1=uri1
          )
          
          # iterate cursor, decoding rows into dicts
          self.polls = [doc for doc in cur.documents]

          if self.polls:
            # fetch tallies from Mongo
            _id = self.polls[0]['uri2']
            self.tallies = mongo.db.tallies.find_one(_id)
            if not self.tallies:
              self.error = jsend.Error('poll does not exist')
          else:
            self.error = jsend.Error('poll does not exist')
        except cassandra.Error as e:
          print e # DEBUG (couldn't acquire lock)
          self.error = jsend.Error('poll not found')
        except mongo.PyMongoError as e:
          print e # DEBUG (couldn't acquire lock)
          self.error = jsend.Error('poll not found')
        except zk.KazooException as e:
          print e # DEBUG (couldn't acquire lock)
          self.error = jsend.Error('poll not found')
        finally:
          lock.release()
    ## 
    return self
       

@view_config(renderer='api_renderer', context=Context)
class View(qtoid.view.View):
 
  def GET(self, req, ctx):
    
    polls = ctx.polls
    tallies = ctx.tallies

    return jsend.Success(data={
      'polls': polls,
      'tallies': tallies,
    })


  def POST(self, req, ctx):

    ok, uri1 = mkuri1(ctx.data)
    if not ok: 
      return uri1 # <- jsend failure
    
    ok, uri2 = mkuri2()
    if not ok: 
      return uri2 # <- jsend error
   
    ok, idx = mkidx(uri1)
    if not ok: 
      return idx  # <- jsend error
    
    # complete URI1 by appending `idx'
    uri1 = '{0}-{1}'.format(uri1, idx)
    
    date  = datetime.now()
    queue = deque([ctx.data])   # <- for the BFS below
    polls = []                  # <- poll ready for C* insertion
    cdx   = 0                   # <- nested poll index counter
    
    # Iterate through nested poll structs in breadth-first-search.
    # `p' is a poll struct, whereas `poll' is a prepared dictionary
    # for insertion to Cassandra.
    while queue:
      p = queue.popleft()
      prompt, tags = mktags(p.get('prompt', ''))
      poll = {
        'prompt'  : prompt,
        'title'   : p.get('title', ''),
        'type'    : p.get('type', 1),
        'date'    : date,
        'required': True if cdx == 0 else p.get('required', False),
        'cdx'     : 0 if cdx == 0 else p.get('cdx'),
        'idx'     : idx,
        'uri1'    : uri1, 
        'uri2'    : uri2, 
        'tags'    : tags, 
        'seq'     : [],
        'adj'     : {},
      }
      choices = p.get('choices')
      if not isinstance(choices, list) or len(choices) < 2:
        return jsend.Failure({
          'choices':'at least 2 unique choices required'
        })
      seq = poll['seq']
      adj = poll['adj']
      ## build seq, adj:
      for i, obj in enumerate(choices): 
        if isinstance(obj, basestring):
          poll['seq'].append(obj)
        elif isinstance(obj, dict):
          k, v = obj.items().pop()
          cdx = cdx + 1
          poll['seq'].append(k)
          poll['adj'][k] = v['cdx'] = cdx
          queue.append(v)
        else:
          return jsend.Failure({'choices': {
            'got invalid choice type at index {0}'.format(i)
          }})
      if len(seq) < len(set(seq)):
        return jsend.Failure({
          'choices':'at least 2 unique choices required'
        })
      polls.append(poll)
    # endwhile

    err = insert(polls) # <- exceptions caught inside
    if err:
      return err
    
    poll = polls[0]
    return jsend.Success(data={
      'uri'     : poll['uri1'],
      'tinyUri' : poll['uri2'],
    })

  
def mktags(text):
  
  ## Extract hash tags as both raw and parsed substrings.
  raw_tags = set(RE_RAW_HTAG1.findall(text) + RE_RAW_HTAG2.findall(text))
  tags = set(RE_HTAG1.findall(text) + RE_HTAG2.findall(text))
  
  ## Replace hash tags in prompt text with safe HTML.
  for raw_tag, tag in itertools.izip(raw_tags, tags):
    href = urlencode(dict(q='#{0}'.format(tag)))
    html = HTAG_SPAN_FSTR.format(href, cgi.escape(tag))
    text = text.replace(raw_tag, html)
  
  return (text, tags)

def mkuri1(data):
  '''
    Builds a cannonical base URI string from the poll title, provided that
    a title is defined; if it is not defined, the string is built using
    the first MAX_URI_LENGTH characters of the prompt text.

    Args:
      - data: JSON decoded POST body, consisting of a recursive 
        poll creation parameter object.

  ''' 
  title  = data.get('title')
  prompt = data.get('prompt')

  ## Make sure root poll has prompt:
  if not isinstance(prompt, basestring):  
    return (False, jsend.Failure({'prompt':'missing or wrong type'}))

  if title:  ## Generate base URI from root title or prompt. 
    uri1 = RE_WHITESPACE.sub('-', RE_NONWORD.sub('', title)).lower()
  else:
    toks = RE_WHITESPACE.split(RE_NONWORD.sub('',prompt[:MAX_URI_LENGTH]))
    uri1 = '-'.join(toks[:-1] if len(toks) > 1 else toks).lower()

  return (True, uri1)


def mkuri2():
  '''
    Generates a "tiny" URI by calling a random string function. We search
    the existing Cassandra polls table for the URI. If it already exists,
    we try a new random string.

    The size of the random string function domain, with length 8, is 
    roughly 250 trillion.

  ''' 
  while True:
    uri2 = randstr(8)
    try:
      data = cassandra.ks.execute('''
        SELECT * FROM polls WHERE uri2 = :uri2;
      ''', uri2=uri2
      ).fetchone()
      if not data:
        break
    except Exception as e:
      return (False, jsend.Error(str(e)))
  return (True, uri2)


def mkidx(uri1):
  '''
    For each base URI build in mkuri1(), we store a counter in a Mongo
    collection. If the base URI already exists in the collection, we 
    increment the counter to yield `idx'. This value says that the 
    current URI1 is the nth copy. The static URL generated for this poll 
    will have the format: <URI1>-<idx>. For example: my-poll-2.

    Args:
      - uri1: the output of mkuri1, used as a primary key to search the
        uris Mongo collection.

  '''
  q = { '_id': uri1 }
  c = { '$inc' : {'idx': 1} }
  try:
    doc = mongo.db.uris.find_and_modify(q, c, upsert=True, new=True)
    return (True, doc['idx'])
  except Exception as e:
    return (False, jsend.Error(str(e))) 


def insert(polls):
  '''
    Tries to insert a new tallies document in Mongo collection as well as
    each poll into the Cassandra polls table. These insertions are wrapped
    in a ZooKeeper lock to simulate a single transaction, which is rolled-
    back if any of the individual insertions fail.

    Args:
      - polls: a list of poll objects prepared by mkpoll() in POST.

  '''
  uri1 = polls[0]['uri1']
  uri = polls[0]['uri2']  # <- primary key for tallies collection & zk lock
  doc = {                 # <- Mongo tallies document
    '_id' : uri
  }
  doc.update({
    str(i) : { s:0 for s in p['seq'] } 
      for i,p in enumerate(polls)
  })

  rollback = True       # <- cancel transaction?
  mongo_error = False   # <- Pymongo exception thrown?
  TIMEOUT = 2           # <- zk lock acquire timeout
  
  ## try insert...
  try:
    zkfs = zk.fs()
    lock = zkfs.lock(('uris', uri1))
    lock.acquire(timeout=TIMEOUT)
    mongo.db.tallies.insert(doc)
    cassandra.ks.insert('polls', polls)
    lock.release()
    rollback = False
  except zk.KazooException as e:
    print e # DEBUG (couldn't acquire lock)
  except mongo.PyMongoError as e:
    print e # DEBUG (couldn't write tallies)
    mongo_error = True
  except cassandra.Error as e:
    print e # DEBUG (couldn't write polls)
  finally:
    lock.release()
  
  ## cancel the transaction
  if rollback: 
    if not mongo_error:
      try:
        mongo.db.tallies.remove(uri)
      except mongo.PyMongoError as e:
        return jsend.Error(str(e))
    else:
      return jsend.Error(str(e))


