
from pyramid.view import view_config

import qtoid.view
import qtoid.context
import polls

class Context(qtoid.context.Context):

  def __init__(self, req, version):
    qtoid.context.Context.__init__(self, req)
    self.version = version
    self.buckets = dict(
      polls=polls.Context
    )
  
  def __getitem__(self, bucket):
    ctx = self.buckets.get(bucket)
    if ctx is not None:
      return ctx(self.req)
    else:
      raise KeyError()


@view_config(context=Context)
class View(qtoid.view.View):
  
  def GET(self): 
    return Response('/api')
