from ConfigParser import ConfigParser
import os

from kazoo.client import KazooClient
from kazoo.fs import FileSystem
from kazoo.recipe.counter import Counter
from kazoo.recipe.lock import Lock
from kazoo.exceptions import KazooException, BadVersionError
from kazoo.retry import ForceRetryError
from kazoo import exceptions

# GLOBALS:
#------------------------------------------------------------------------------#

CONFIG = ConfigParser()
CONFIG.read('zk.ini')

PATH_TO_ROOT = CONFIG.get('paths', 'root')
PATH_TO_POLLS = os.path.join(PATH_TO_ROOT, CONFIG.get('paths', 'polls'))

CLIENT = KazooClient(hosts=CONFIG.get('deployment', 'hosts'))

#------------------------------------------------------------------------------#
fs = lambda: FileSystem(CLIENT, PATH_TO_ROOT)

