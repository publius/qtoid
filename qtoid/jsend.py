from pyramid.response import Response

class Success(dict):
  
  def __init__(self, data={}):
    dict.__init__(self, dict(
      status='success',
      data=data,
    ))

class Failure(dict):
  
  def __init__(self, data={}):
    dict.__init__(self, dict(
      status='fail',
      data=data,
    ))

class Error(dict):
  
  def __init__(self, message, code={}, data={}):
    dict.__init__(self, dict(
      status='error',
      data=data,
      code=code,
      message=message
    ))
